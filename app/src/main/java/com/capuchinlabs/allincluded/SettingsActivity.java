package com.capuchinlabs.allincluded;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {
    public static boolean IsActive = false;
    private EditText _txtUsername;
    private EditText _txtMessageColor;
    private EditText _txtRoomName;
    private TextView _txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        _txtRoomName = findViewById(R.id.txtRoomName);
        _txtUsername = findViewById(R.id.txtUsername);
        _txtMessageColor = findViewById(R.id.txtColor);
        _txtError = findViewById(R.id.txtError);

        SharedPreferences sharedPreferences = this.getSharedPreferences(Settings.SHAREDPREF, Context.MODE_PRIVATE);
        if (Settings.Nickname.length() == 0) {
            Settings.Nickname = sharedPreferences.getString(Settings.USERNAME_PREF, Settings.Nickname);
        }
        if (Settings.MessageColor.length() == 0) {
            Settings.MessageColor = sharedPreferences.getString(Settings.MESSAGE_COLOR_PREF, Settings.MessageColor);
        }
        if (Settings.RoomName.length() == 0) {
            Settings.RoomName = sharedPreferences.getString(Settings.ROOM_NAME_PREF, Settings.RoomName);
        }

        _txtRoomName.setText(Settings.RoomName);
        _txtUsername.setText(Settings.Nickname);
        _txtMessageColor.setText(Settings.MessageColor);
    }

    public void btnSave_Click(View view) {
        SharedPreferences sharedPreferences = this.getSharedPreferences(Settings.SHAREDPREF, Context.MODE_PRIVATE);
        String roomName = _txtRoomName.getText().toString().trim();
        String username = _txtUsername.getText().toString().trim();
        String messageColor = _txtMessageColor.getText().toString().trim();
        boolean errorOccurred = false;

        _txtError.setText("");

        if (Settings.isValidRoomName(roomName)) {
            Settings.RoomName = roomName;
            sharedPreferences.edit().putString(Settings.ROOM_NAME_PREF, roomName).apply();
        }
        else {
            errorOccurred = true;
            _txtError.setText(R.string.settings_invalid_roomname);
        }

        if (Settings.isValidUsername(username)) {
            Settings.Nickname = username;
            sharedPreferences.edit().putString(Settings.USERNAME_PREF, username).apply();
        }
        else {
            errorOccurred = true;
            _txtError.setText(R.string.settings_invalid_usernname);
        }

        if (Settings.isValidColor(messageColor)) {
            Settings.MessageColor = messageColor;
            sharedPreferences.edit().putString(Settings.MESSAGE_COLOR_PREF, messageColor).apply();
        }
        else {
            errorOccurred = true;
            _txtError.setText(R.string.settings_invalid_color);
        }

        if (!errorOccurred) {
            IsActive = false;
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        IsActive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        IsActive = false;
    }
}
