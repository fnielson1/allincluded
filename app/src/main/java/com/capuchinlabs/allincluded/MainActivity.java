package com.capuchinlabs.allincluded;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.capuchinlabs.allincluded.messenger.FirebaseMessenger;
import com.capuchinlabs.allincluded.messenger.lib.IMessageListener;
import com.capuchinlabs.allincluded.messenger.lib.IMessenger;
import com.capuchinlabs.allincluded.messenger.lib.Message;
import com.capuchinlabs.allincluded.speech.ISpeechListener;
import com.capuchinlabs.allincluded.speech.SpeechRecognitionListener;


public class MainActivity extends AppCompatActivity implements ISpeechListener, IMessageListener {

    private static final int REQUEST_RECORD_PERMISSION = 100;
    private final String LOG_TAG = "VoiceRecognitionActivit";
    private EditText txtEditText;
    private MessageAdapter messageAdapter;
    private ListView lvMessagesView;

    private ToggleButton btnSpeechListenOnOff;
    private ProgressBar pbSpeechInput;
    private SpeechRecognitionListener _speechRecognitionListener;
    private boolean _speechListenerDone = false;
    private boolean _uiSetupDone = false;
    private IMessenger _messenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtEditText = findViewById(R.id.editText);
        lvMessagesView = findViewById(R.id.messages_view);
        btnSpeechListenOnOff = findViewById(R.id.btnSpeechListenOnOff);
        pbSpeechInput = findViewById(R.id.pbSpeechInput);
    }

    public void refresh() {
        if (Settings.isValidUsername(Settings.Nickname)) {
//        _messenger = new ScaleDroneMessenger(_roomName);
            _messenger = FirebaseMessenger.getInstance(this, Settings.RoomName);

            uiSetup();
            speechRecognitionSetup();

            this.setTitle(Settings.RoomName + " - " + Settings.Nickname);
            btnSpeechListenOnOff.setChecked(true);
        }
        else {
            showSettings();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSettings() {
        if (!SettingsActivity.IsActive) {
            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
    }

    private void uiSetup() {
        if (!_uiSetupDone) {
            _uiSetupDone = true;
            messageAdapter = new MessageAdapter(this);
            lvMessagesView.setAdapter(messageAdapter);
        }
        _messenger.addListener(this);
        _messenger.refresh();
    }

    /**
     * Speech recognition
     */
    private void speechRecognitionSetup() {
        if (!_speechListenerDone) {
            _speechListenerDone = true;
            _speechRecognitionListener = SpeechRecognitionListener.createSpeechListener(this);
            _speechRecognitionListener.addListener(this);

            pbSpeechInput.setMax(10);

            btnSpeechListenOnOff.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    pbSpeechInput.setVisibility(View.VISIBLE);
                    pbSpeechInput.setIndeterminate(true);
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_PERMISSION);
                } else {
                    pbSpeechInput.setIndeterminate(false);
                    pbSpeechInput.setVisibility(View.INVISIBLE);
                    _speechRecognitionListener.stopListening();
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    _speechRecognitionListener.startListening();
                } else {
                    Toast.makeText(MainActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (_speechRecognitionListener != null) {
            _speechRecognitionListener.stopListening();
            Log.i(LOG_TAG, "destroy");
        }
    }

    @Override
    public void onSpeechMessageComposed(String msg) {
        sendMessage(msg);
    }

    @Override
    public void rmsValueChanged(int value) {
        pbSpeechInput.setProgress(value);
    }

    @Override
    public void isListeningChanged(boolean isListening) {
        pbSpeechInput.setIndeterminate(!isListening);
    }

    @Override
    public void isRunningChanged(boolean isRunning) {
        btnSpeechListenOnOff.setChecked(isRunning);
    }

    public void sendMessage(String message) {
        if (message.length() > 0) {
            _messenger.publish(Settings.RoomName, message);
            txtEditText.getText().clear();
        }
    }

    public void sendMessage_Click(View view) {
        String message = txtEditText.getText().toString();
        sendMessage(message);
    }

    @Override
    public void onMessageReceived(Message message) {
        runOnUiThread(() -> {
            messageAdapter.add(message);
            lvMessagesView.setSelection(lvMessagesView.getCount() - 1);
        });
    }
}
