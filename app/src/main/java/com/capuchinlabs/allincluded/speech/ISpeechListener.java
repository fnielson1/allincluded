package com.capuchinlabs.allincluded.speech;

public interface ISpeechListener {
    void onSpeechMessageComposed(String msg);
    void rmsValueChanged(int value);
    void isListeningChanged(boolean isListening);
    void isRunningChanged(boolean isRunning);
}
