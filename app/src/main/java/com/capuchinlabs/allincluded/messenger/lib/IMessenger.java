package com.capuchinlabs.allincluded.messenger.lib;

public interface IMessenger {
    void publish(String roomName, String msg);
    void refresh();
    void addListener(IMessageListener listener);
    void removeListener(IMessageListener listener);
}
