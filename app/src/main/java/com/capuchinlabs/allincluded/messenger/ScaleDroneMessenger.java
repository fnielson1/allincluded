package com.capuchinlabs.allincluded.messenger;

import com.capuchinlabs.allincluded.Settings;
import com.capuchinlabs.allincluded.messenger.lib.IMessageListener;
import com.capuchinlabs.allincluded.messenger.lib.IMessenger;
import com.capuchinlabs.allincluded.messenger.lib.MemberData;
import com.capuchinlabs.allincluded.messenger.lib.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scaledrone.lib.Listener;
import com.scaledrone.lib.Member;
import com.scaledrone.lib.Room;
import com.scaledrone.lib.RoomListener;
import com.scaledrone.lib.Scaledrone;

import java.util.ArrayList;
import java.util.Random;

public class ScaleDroneMessenger implements RoomListener, IMessenger {
    private final String channelID = "pyS9N75MYSezmt2r";
    private Scaledrone scaledrone;
    private String _roomName;
    private ArrayList<IMessageListener> _listeners = new ArrayList<>();

    public ScaleDroneMessenger(String roomName) {
        _roomName = roomName;
    }

    public void addListener(IMessageListener listener) {
        if (!_listeners.contains(listener))
            _listeners.add(listener);
    }

    public void removeListener(IMessageListener listener) {
        _listeners.remove(listener);
    }

    public void refresh() {
        if (scaledrone != null) {
            scaledrone.close();
        }

        MemberData data = new MemberData(Settings.Nickname, getRandomColor());
        scaledrone = new Scaledrone(channelID, data);
        scaledrone.connect(new Listener() {
            @Override
            public void onOpen() {
                System.out.println("Scaledrone connection open");
                scaledrone.subscribe(_roomName, ScaleDroneMessenger.this);
            }

            @Override
            public void onOpenFailure(Exception ex) {
                System.err.println(ex.toString());
            }

            @Override
            public void onFailure(Exception ex) {
                System.err.println(ex.toString());
            }

            @Override
            public void onClosed(String reason) {
                System.err.println(reason);
            }
        });
    }

    @Override
    public void onOpen(Room room) {
        System.out.println("Connected to room");
    }

    @Override
    public void onOpenFailure(Room room, Exception ex) {
        System.err.println(ex.toString());
    }

    @Override
    public void onMessage(Room room, com.scaledrone.lib.Message receivedMessage) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            Member member = receivedMessage.getMember();
            if (member != null) {
                final MemberData data = mapper.treeToValue(member.getClientData(), MemberData.class);
                boolean belongsToCurrentUser = receivedMessage.getClientID().equals(scaledrone.getClientID());
                final Message message = new Message(receivedMessage.getData().asText(), data, belongsToCurrentUser);
                for (IMessageListener listener : _listeners) {
                    listener.onMessageReceived(message);
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(String roomName, String msg) {
        scaledrone.publish(roomName, msg);
    }

    private String getRandomColor() {
        Random r = new Random();
        StringBuilder sb = new StringBuilder("#");
        while(sb.length() < 7){
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, 7);
    }
}
