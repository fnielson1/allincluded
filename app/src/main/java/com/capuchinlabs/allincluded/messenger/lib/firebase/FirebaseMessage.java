package com.capuchinlabs.allincluded.messenger.lib.firebase;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class FirebaseMessage {
    public String content;
    public long timestamp;
    public String uid;


    public FirebaseMessage() {

    }

    public FirebaseMessage(String uid, String content, long timestamp) {
        this.content = content;
        this.timestamp = timestamp;
        this.uid = uid;
    }

    @Exclude
    public Map<String, HashMap<String, Object>> toMap() {
        HashMap<String, Object> message = new HashMap<>();
        HashMap<String, HashMap<String, Object>> messagePacket = new HashMap<>();

        message.put("content", content);
        message.put("timestamp", timestamp);
        message.put("uid", uid);
        messagePacket.put(uid, message);

        return messagePacket;
    }
}
