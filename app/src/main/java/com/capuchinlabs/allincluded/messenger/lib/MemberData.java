package com.capuchinlabs.allincluded.messenger.lib;

public class MemberData {
    private String name;
    private String color;

    public MemberData() {
    }

    public MemberData(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "MemberData{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}