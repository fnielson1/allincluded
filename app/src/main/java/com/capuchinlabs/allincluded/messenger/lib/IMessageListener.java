package com.capuchinlabs.allincluded.messenger.lib;

public interface IMessageListener {
    void onMessageReceived(Message message);
}
