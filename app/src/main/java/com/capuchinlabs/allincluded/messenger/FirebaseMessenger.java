package com.capuchinlabs.allincluded.messenger;

import com.capuchinlabs.allincluded.Settings;
import com.capuchinlabs.allincluded.messenger.lib.firebase.FirebaseMessage;
import com.capuchinlabs.allincluded.messenger.lib.IMessageListener;
import com.capuchinlabs.allincluded.messenger.lib.IMessenger;
import com.capuchinlabs.allincluded.messenger.lib.MemberData;
import com.capuchinlabs.allincluded.messenger.lib.Message;
import com.capuchinlabs.allincluded.messenger.lib.firebase.FirebaseUser;
import com.firebase.client.Firebase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class FirebaseMessenger implements IMessenger {
    private static FirebaseMessenger _instance;
    private final FirebaseDatabase _database = FirebaseDatabase.getInstance();
    private HashMap<String, FirebaseUser> _uidsAndUsers = new HashMap<>();
    private ArrayList<IMessageListener> _listeners = new ArrayList<>();
    private DatabaseReference _messagesRef;
    private DatabaseReference _roomUsersRef;
    private AppCompatActivity _context;
    private String _roomName;
    private boolean _setupDone;

    public static FirebaseMessenger getInstance(AppCompatActivity context, String roomName) {
        if (FirebaseMessenger._instance != null) {
            if (FirebaseMessenger._instance._context != context || !FirebaseMessenger._instance._roomName.equals(roomName)) {
                FirebaseMessenger._instance.removeAllListeners(); // Don't leave hanging listeners
                FirebaseMessenger._instance = new FirebaseMessenger(context, roomName);
            }
        }
        else
            FirebaseMessenger._instance = new FirebaseMessenger(context, roomName);
        return FirebaseMessenger._instance;
    }

    private FirebaseMessenger(AppCompatActivity context, String roomName) {
        _context = context;
        _roomName = roomName;
        Firebase.setAndroidContext(context);
        _roomUsersRef = _database.getReference(String.format("rooms/%s/users", _roomName));
    }

    public String getCurrentRoom() {
        return _roomName;
    }

    private String getMessageNickname(String uid) {
        FirebaseUser user = getMessageUser(uid);
        if (user != null)
            return user.nickname;
        return "Unknown";
    }

    private String getMessageColor(String uid) {
        FirebaseUser user = getMessageUser(uid);
        if (user != null)
            return user.color;
        return "amber"; // If no user found, assign a specific color
    }

    private FirebaseUser getMessageUser(String uid) {
        if (_uidsAndUsers.containsKey(uid))
            return _uidsAndUsers.get(uid);
        return null;
    }

    private void joinRoom() {
        if (!_setupDone) {
            _setupDone = true;
            setupRoomUsers();
            setupRoomMessages();
        }
    }

    private void onMessageReceived(@NonNull DataSnapshot dataSnapshot) {
        FirebaseMessage firebaseMessage = dataSnapshot.getValue(FirebaseMessage.class);
        if (firebaseMessage != null) {
            MemberData memberData = new MemberData(getMessageNickname(firebaseMessage.uid), getMessageColor(firebaseMessage.uid));
            boolean belongsToCurrentUser = Settings.getUserId().equals(firebaseMessage.uid);
            Message message = new Message(firebaseMessage.content, memberData, belongsToCurrentUser);
            for (IMessageListener listener : _listeners) {
                listener.onMessageReceived(message);
            }
        }
    }

    private void updateRoomUserInfo() {
        FirebaseUser profile = new FirebaseUser(Settings.getUserId(), Settings.Nickname, Settings.MessageColor);
        _roomUsersRef.setValue(profile.toMap());
    }

    private void setupRoomUsers() {
        updateRoomUserInfo();

        // Attach a listener to read the data at our posts reference
        _roomUsersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    FirebaseUser user = userSnapshot.getValue(FirebaseUser.class);
                    if (user != null && !_uidsAndUsers.containsKey(user.uid))
                        _uidsAndUsers.put(user.uid, user);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    private void setupRoomMessages() {
        _messagesRef = _database.getReference(String.format("messages/%s", Settings.RoomName));
        _messagesRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String prevKey) {
                onMessageReceived(dataSnapshot);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                onMessageReceived(dataSnapshot);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void refresh() {
        joinRoom();
    }

    @Override
    public void publish(String roomName, String msg) {
        FirebaseMessage message = new FirebaseMessage(Settings.getUserId(), msg, System.currentTimeMillis());
        _messagesRef.setValue(message.toMap());
    }

    public void addListener(IMessageListener listener) {
        if (!_listeners.contains(listener))
            _listeners.add(listener);
    }

    public void removeListener(IMessageListener listener) {
        _listeners.remove(listener);
    }

    public void removeAllListeners() {
        _listeners.removeAll(_listeners);
    }
}
