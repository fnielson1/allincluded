package com.capuchinlabs.allincluded.messenger.lib.firebase;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class FirebaseUser {
    public String nickname;
    public String uid;
    /**
     * The color that the User has chosen for their messages
     */
    public String color;

    public FirebaseUser() {

    }

    public FirebaseUser(String uid, String nickname, String color) {
        this.color = color;
        this.nickname = nickname;
        this.uid = uid;
    }

    @Exclude
    public Map<String, HashMap<String, Object>> toMap() {
        HashMap<String, Object> user = new HashMap<>();
        HashMap<String, HashMap<String, Object>> result = new HashMap<>();

        user.put("color", color);
        user.put("nickname", nickname);
        user.put("uid", uid);
        result.put(uid, user);

        return result;
    }
}
