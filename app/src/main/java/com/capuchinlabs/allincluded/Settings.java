package com.capuchinlabs.allincluded;

import android.graphics.Color;

import com.google.firebase.auth.FirebaseUser;

public class Settings {
    static final String SHAREDPREF = "com.capuchinlabs.allincluded";
    static final String USERNAME_PREF = "Nickname";
    static final String MESSAGE_COLOR_PREF = "MessageColor";
    static final String ROOM_NAME_PREF = "RoomName";
    public static String Nickname = "";
    public static String MessageColor = "";
    public static String RoomName = "";
    static FirebaseUser User = null;

    public static String getUserId() {
        if (User != null) {
            return User.getUid();
        }
        return "";
    }

    static boolean isValidRoomName(String roomName) {
        return roomName != null && roomName.length() > 0;
    }

    /**
     * @param username The username
     * @return Returns true if the Nickname is a valid one
     */
    static boolean isValidUsername(String username) {
        return username != null && username.length() > 0;
    }

    static boolean isValidColor(String color) {
        try {
            if (color != null) {
                // color is a valid color
                Color.parseColor(color);
            }
        } catch (IllegalArgumentException iae) {
            // This color string is not valid
            return false;
        }
        return true;
    }
}
