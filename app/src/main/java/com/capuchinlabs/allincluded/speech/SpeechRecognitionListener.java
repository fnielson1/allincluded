package com.capuchinlabs.allincluded.speech;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import java.util.ArrayList;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;


public class SpeechRecognitionListener implements RecognitionListener {
    private static volatile boolean _requestStop = false;
    private static volatile SpeechRecognizer _speech = null;
    private static SpeechRecognitionListener _speechRecognitionListener;
    private static ArrayList<ISpeechListener> _listeners = new ArrayList<>();
    private static String _filterCurseWordsBoundaries = "(?i)(ass|hell)((?=\\s)|$)";
    private static String _filterCurseWords = "(?i)(fuck|shit|damn|god|jesus|christ)";
    private static String _filterReplacement = "%#^!";
    private String LOG_TAG = "VoiceRecognitionActivity";
    private AppCompatActivity _context;
    private int _maxResults = 1;

    public static SpeechRecognitionListener createSpeechListener(AppCompatActivity context) {
        if (_speechRecognitionListener != null)
            _speechRecognitionListener.stopListening();
        _speechRecognitionListener = new SpeechRecognitionListener(context);
        return _speechRecognitionListener;
    }

    private SpeechRecognitionListener(AppCompatActivity context) {
        _speechRecognitionListener = this;
        _context = context;
    }

    public void addListener(ISpeechListener listener) {
        if (!_listeners.contains(listener))
            _listeners.add(listener);
    }

    public void removeListener(ISpeechListener listener) {
        _listeners.remove(listener);
    }

    public void startListening() {
        _requestStop = false;
//        _context.isRunningChanged(true);
        internalStartListening();
    }

    private void internalStartListening() {
        if (!_requestStop) {
            internalStopListening();

            _speech = SpeechRecognizer.createSpeechRecognizer(_context);
            SpeechRecognitionListener listener = new SpeechRecognitionListener(_context);
            _speech.setRecognitionListener(listener);
            Intent recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.ENGLISH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, _maxResults);
            _speech.startListening(recognizerIntent);
        }
    }

    public void stopListening() {
        _requestStop = true;
        for (ISpeechListener listener : _listeners) {
            listener.isRunningChanged(false);
        }
        internalStopListening();
    }

    private void internalStopListening() {
        if (_speech != null) {
            _speech.stopListening();
            _speech.destroy();
            _speech = null;
        }
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
        for (ISpeechListener listener : _listeners) {
            listener.isListeningChanged(true);
        }
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(LOG_TAG, "onEndOfSpeech");
        for (ISpeechListener listener : _listeners) {
            listener.isListeningChanged(false);
        }
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(LOG_TAG, "FAILED " + errorMessage);
        switch (errorCode) {
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
            case SpeechRecognizer.ERROR_NETWORK:
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
            case SpeechRecognizer.ERROR_SERVER:
                internalStopListening();
                break;
            default:
                if (_requestStop)
                    internalStopListening();
                else
                    internalStartListening();
        }
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Log.i(LOG_TAG, "onEvent");
    }

    @Override
    public void onPartialResults(Bundle arg0) {
        Log.i(LOG_TAG, "onPartialResults");
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        StringBuilder text = new StringBuilder(_maxResults);
        if (matches != null) {
            for (int i = 0; i < _maxResults; i++) {
                text.append(matches.get(i));
                // Don't end our message with a newline
                if (i + 1 < _maxResults)
                    text.append('\n');
            }
        }
        for (ISpeechListener listener : _listeners) {
            listener.onSpeechMessageComposed(filterResults(text.toString()));
        }
        internalStartListening();
    }

    private String filterResults(String msg) {
        msg = msg.replaceAll(_filterCurseWordsBoundaries, _filterReplacement).replaceAll(_filterCurseWords, _filterReplacement);
        return msg;
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
        for (ISpeechListener listener : _listeners) {
            listener.rmsValueChanged((int)rmsdB);
        }
    }

    private static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No _speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }
}
